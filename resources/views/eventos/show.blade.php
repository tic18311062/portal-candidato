@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="con-titulo">
                <h1 class="tuclase">{{ $datos['evento']->titulo }}</h1>
            </div>
            @foreach ($datos['img_eventos'] as $img_evento)
                    @if ($img_evento->orde==1)
                        <img class="img-descrp" src="{{ $img_evento->ruta_imagen }}" alt="">
                    @endif
                    <p style="line-height: calc(1em + 3px);">{{ substr($datos['evento']->descripcion,0,255) }}</p>
                    @if ($img_evento->order>1)
                        <img class="img-descrp" src="{{ $img_evento->ruta_imagen }}" alt="">
                    @endif
                    <p style="line-height: calc(1em + 3px);">{{ substr($datos['evento']->descripcion,255) }}</p>
            @endforeach
        </div>
    </div>
@endsection
