<div id="carouselId" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselId" data-slide-to="0" class="active"></li>
        <li data-target="#carouselId" data-slide-to="1"></li>
        <li data-target="#carouselId" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
            <img data-src="holder.js/900x500/auto/#777:#555/text:First slide" alt="First slide">
            <div class="carousel-caption d-none d-md-block">
                <h3>Title</h3>
                <p>Description</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container">
        @foreach ($datos['noticias'] as $notica)
            <div class="card" style="width: 18rem;">
                @foreach ($datos['img_noticias'] as $img_notica)
                    @if ( $img_notica->order==1)
                        <img src="{{ $img_notica->ruta_imagen }}" class="card-img-top" alt="...">
                    @endif
                @endforeach
                <div class="card-body">
                <h5 class="card-title">{{ $notica->titulo }}</h5>
                <p class="card-text">{{ substr($notica->descripcion,0,50) }}</p>
                <a href="{{ route('noticias.show',$notica->id )}}" class="btn btn-primary">{{ $notica->fecha_noticia }}</a>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="container-fluid">
    <div class="container">
        @foreach ($datos['eventos'] as $eventos)
            <div class="card" style="width: 18rem;">
                @foreach ($datos['img_eventos'] as $img_eventos)
                    @if ( $img_eventos->order==1)
                        <img src="{{ $img_eventos->ruta_imagen }}" class="card-img-top" alt="...">
                    @endif
                @endforeach
                <div class="card-body">
                <h5 class="card-title">{{ $eventos->titulo }}</h5>
                <p class="card-text">{{ substr($eventos->descripcion,0,50) }}</p>
                <a href="{{ route('noticias.show',$eventos->id )}}" class="btn btn-primary">{{ $eventos->fecha_evento }}</a>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
