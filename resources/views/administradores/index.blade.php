@extends('layouts.app')
@section('content')
    <div class="cont-todo">
        <div class="cont1">
            <!--usuarios-->
            <div class="sep">
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#contUsuarios" aria-expanded="false"
                            aria-controls="contUsuarios">
                        Usuarios
                    </button>
                </p>
                <div class="collapse" id="contUsuarios">
                        <table class="table table-striped table-inverse table-responsive-sm" style="max-width:100%; height:auto;">
                            <thead class="thead-inverse">
                                <tr>
                                    <th scope="col">id</th>
                                    <th scope="col">name</th>
                                    <th scope="col">email</th>
                                    <th scope="col">tipo</th>
                                    <th scope="col">celular</th>
                                    <th scope="col">opcion</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @php
                                     $usuarios = [];
                                     @endphp

                                    @foreach ($datos['users'] as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->tipo }}</td>
                                        <td>{{ $user->celular }}</td>
                                        <td>
                                            <p>
                                                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#modifi-usuarios" aria-expanded="false"
                                                        aria-controls="modifi-usuarios" value="{{$user->id}}" onclick="cargarUs(this.value);">
                                                    <img src="{{ asset('img/opcion-editar.png') }}"  alt="">
                                                </button>
                                            </p>
                                        </td>
                                        @php
                                         $usuarios[$user->id] = $user;
                                        @endphp
                                    </tr>
                                    @endforeach
                                    <script>
                                            function cargarArreglo(){
                                                return <?php echo json_encode($usuarios); ?>;
                                            }
                                            function cargarUs(idUs)
                                            {
                                               //alert(idUs);
                                               console.log('en metodo '+idUs);
                                               var usuarios = cargarArreglo();
                                                console.log(usuarios);
                                               document.getElementById('idModificarUs').value=usuarios[idUs]["id"];
                                               document.getElementById('nameModificarUs').value=usuarios[idUs]["name"];
                                               document.getElementById('emailModificarUs').value=usuarios[idUs]["email"];
                                               document.getElementById('celularModificarUs').value=usuarios[idUs]["celular"];
                                               var select = document.getElementById("tipoModificarUs");
                                               if(usuarios[idUs]["tipo"]=='ADMIN'){
                                                   document.getElementById("tipoModificarUs").selectedIndex = 0;
                                                   select[0].selected = true;
                                                  // alert('select'+ document.getElementById("tipoModificarUs").selectedIndex)
                                                }
                                                else if(usuarios[idUs]["tipo"]=='EDITOR')
                                                {
                                                    document.getElementById("tipoModificarUs").selectedIndex = "1";
                                                    select[1].selected = true;
                                                }
                                                else if(usuarios[idUs]["tipo"]=='PUBLICO')
                                                {
                                                    document.getElementById("tipoModificarUs").selectedIndex = "2";
                                                    select[2].selected = true;
                                                  //  alert('select'+ document.getElementById("tipoModificarUs").selectedIndex)
                                               }
                                               //modificar el action del form
                                               var url = "{{ route('user.update',':id') }}";
                                               url = url.replace(':id',idUs);
                                               $("#frmModificarUs").attr("action",url);
                                            }
                                     </script>
                            </tbody>
                        </table>
                    <div class="collapse" id="modifi-usuarios">
                        <form action="" method="POST" id="frmModificarUs">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                            <label for="id">id</label>
                            <input type="text" class="form-control" name="id" id="idModificarUs" placeholder="id" disabled>
                            </div>
                            <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" name="name" id="nameModificarUs" placeholder="nombre" disabled>
                            </div>
                            <div class="form-group">
                            <label for="email">email</label>
                            <input type="email" class="form-control" name="email" id="emailModificarUs"  placeholder="email" disabled>
                            </div>
                            <div class="form-group">
                            <label for="tipo">tipo</label>
                            <select class="form-control" name="tipo" id="tipoModificarUs">
                                <option value="ADMIN">ADMIN</option>
                                <option value="EDITOR" >EDITOR</option>
                                <option value="PUBLICO">PUBLICO</option>
                            </select>
                            </div>
                            <div class="form-group">
                            <label for="celular">celular</label>
                            <input type="text" class="form-control" name="celular" id="celularModificarUs" placeholder="celular" disabled>
                            </div>
                            <button type="submit" class="btn btn-primary">modificar</button>
                        </form>
                    </div>
                </div>
            </div>
            <!--Historia-->
            <div class="sep">
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-historias" aria-expanded="false"
                                aria-controls="cont-historia">
                            Historia
                        </button>
                    </p>
                    <div class="collapse" id="cont-historias">
                        @if ($datos['historias']->count()>0)
                            @foreach ( $datos['historias'] as $historia)
                                <form action="{{ route('historias.update',1) }}" method="POST">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                    <label for="id">id</label>
                                    <input type="text" class="form-control" name="id" id="id" value="{{ $historia->id }}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="titulo">titulo</label>
                                        <input type="text" class="form-control" name="titulo" id="titulo" value="{{ $historia->titulo }}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="descripcion">descripcion</label>
                                        <textarea class="form-control" name="descripcion" id="descripcion" rows="10" cols="13" style="width:100%;height:auto;">{{ $historia->descripcion }}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">subir</button>
                                </form>
                            @endforeach
                        @else
                        <form action="{{ route('historias.store') }}" method="POST">
                                @csrf
                                @method('PATCH')
                                <div class="form-group">
                                <label for="id">id</label>
                                <input type="text" class="form-control" name="id" id="id">
                                </div>
                                <div class="form-group">
                                    <label for="titulo">titulo</label>
                                    <input type="text" class="form-control" name="titulo" id="titulo">
                                </div>
                                <div class="form-group">
                                    <label for="descripcion">descripcion</label>
                                    <textarea class="form-control" name="descripcion" id="descripcion" rows="10" cols="13" style="width:100%;height:auto;"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">subir</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
            <!--Imagenes_Historia-->
            <div class="sep">
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-img-historia" aria-expanded="false"
                            aria-controls="cont-img-historia">
                        imagenes historia
                    </button>
                </p>
                <div class="collapse" id="cont-img-historia">
                    <form action="{{ route('imagen_historia.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <label for="ruta_imagen">imagen</label>
                          <input type="file" class="form-control-file" name="ruta_imagen" id="ruta_imagen" placeholder="porfavor de incluir la imagen">
                        </div>
                        <div class="form-group">
                          <label for="orden_imagen">Orden</label>
                          <input type="text" class="form-control" name="orden_imagen" id="orden_imagen" placeholder="orden">
                        </div>
                        <button type="submit" class="btn btn-primary">subir</button>
                    </form>
                </div>
            </div>
            <!--Noticias-->
            <div class="sep">
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-noticias" aria-expanded="false"
                            aria-controls="cont-noticias">
                        Noticias
                    </button>
                </p>
                <div class="collapse" id="cont-noticias">
                    <form action="{{ route('noticias.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="titulo">titulo</label>
                            <input type="text" class="form-control" name="titulo" id="titulo" placeholder="titulo">
                        </div>
                        <input type="date" name="fecha_noticia" id="fecha_noticia"><br>
                        <div class="form-group">
                            <label for="descripcion">descripcion</label>
                            <textarea class="form-control" name="descripcion" id="descripcion" rows="10"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">subir</button>
                    </form>
                </div>
            </div>
            <!--Imagenes_Noticias-->
            <div class="sep">
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-img-noticias" aria-expanded="false"
                            aria-controls="cont-img-noticias">
                        imagenes noticias
                    </button>
                </p>
                <div class="collapse" id="cont-img-noticias">
                    <form action="{{ route('imagen_noticias.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="noticia_id">Selecciona la noticia:</label>
                            <select class="custom-select" name="noticia_id" id="noticia_id">
                                <option selected>  </option>
                                @foreach ($datos['noticias'] as $noticias)
                                    <option value="{{ $noticias->id }}">{{ $noticias->titulo }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ruta_imagen">imagen</label>
                            <input type="file" class="form-control-file" name="ruta_imagen" id="ruta_imagen" placeholder="porfavor de incluir la imagen">
                        </div>
                        <div class="form-group">
                            <label for="order">Orden</label>
                            <input type="text" class="form-control" name="order" id="order" placeholder="orden">
                        </div>
                        <button type="submit" class="btn btn-primary">subir</button>
                    </form>
                </div>
            </div>
            <!--Eventos-->
            <div class="sep">
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-eventos" aria-expanded="false"
                            aria-controls="cont-eventos">
                        Eventos
                    </button>
                </p>
                <div class="collapse" id="cont-eventos">
                    <form action="{{ route('eventos.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="titulo">titulo</label>
                            <input type="text" class="form-control" name="titulo" id="titulo" placeholder="titulo">
                        </div>
                        <input type="date" name="fecha_evento" id="fecha_noticia"><br>
                        <div class="form-group">
                            <label for="descripcion">descripcion</label>
                            <textarea class="form-control" name="descripcion" id="descripcion" rows="10"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">subir</button>
                    </form>
                </div>
            </div>
            <!--Imagenes_Eventos-->
            <div class="sep">
                <p>
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-img-eventos" aria-expanded="false"
                            aria-controls="cont-img-eventos">
                        imagenes Eventos
                    </button>
                </p>
                <div class="collapse" id="cont-img-eventos">
                    <form action="{{ route('imagen_eventos.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="evento_id">id evento</label>
                            <select class="form-control" name="evento_id" id="evento_id">
                                    @foreach ($datos['eventos'] as $eventos)
                                        <option value="{{ $eventos->id }}">{{ $eventos->titulo }}</option>
                                    @endforeach
                              </select>
                        </div>
                        <div class="form-group">
                            <label for="ruta_imagen">imagen</label>
                            <input type="file" class="form-control-file" name="ruta_imagen" id="ruta_imagen" placeholder="porfavor de incluir la imagen">
                        </div>
                        <div class="form-group">
                            <label for="orden_imagen">Orden</label>
                            <input type="text" class="form-control" name="orden_imagen" id="orden_imagen" placeholder="orden">
                        </div>
                        <button type="submit" class="btn btn-primary">subir</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="cont-todo">
            <div class="cont1">
                <!--Eliminar Imagenes_Historia-->
                <div class="sep">
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-eliminar-img-historia" aria-expanded="false"
                                aria-controls="cont-img-historia">
                            eliminar imagenes historia
                        </button>
                    </p>
                    <div class="collapse" id="cont-eliminar-img-historia">
                        <table class="table table-sm table-inverse table-responsive">
                            <thead class="thead-default">
                                <tr>
                                    <th>id</th>
                                    <th>ruta</th>
                                    <th>order</th>
                                    <th>opcion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($datos['img_historias'] as $img_historia)
                                            <tr>
                                                <td scope="row">{{ $img_historia->id }}</td>
                                                <td> <img src="{{ asset($img_historia->ruta_imagen) }}" alt="" width="50px" height="50px"></td>
                                                <td>{{ $img_historia->id}}</td>
                                                <td>
                                                    <form action="{{ route('imagen_historia.destroy', $img_historia->id ) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input type="hidden" name="id" value="{{ $img_historia->id }}">
                                                        <button type="submit" class="btn btn-primary">Eliminar</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                    </div>
                </div>
                <!--Eliminar Noticias-->
                <div class="sep">
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-eliminar-noticias" aria-expanded="false"
                                aria-controls="cont-eliminar-noticias">
                            Eliminar Noticias
                        </button>
                    </p>
                    <div class="collapse" id="cont-eliminar-noticias">
                        <table class="table table-sm table-inverse table-responsive">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>id</th>
                                    <th>titulo</th>
                                    <th>opcion</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($datos['noticias'] as $noticia)
                                        <tr>
                                            <td scope="row">{{ $noticia->id}}</td>
                                            <td>{{ $noticia->titulo }}</td>
                                            <td>
                                                <form action="{{ route('noticias.destroy', $noticia->id ) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input type="hidden" name="id" value="{{ $noticia->id }}">
                                                    <button type="submit" class="btn btn-primary">Eliminar</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
                <!--Eliminar Eventos-->
                <div class="sep">
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#cont-eliminar-eventos" aria-expanded="false"
                                aria-controls="cont-eliminar-eventos">
                            Eliminar Eventos
                        </button>
                    </p>
                    <div class="collapse" id="cont-eliminar-eventos">
                        <table class="table table-sm table-inverse table-responsive">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>id</th>
                                    <th>titulo</th>
                                    <th>opcion</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($datos['eventos'] as $evento)
                                        <tr>
                                            <td scope="row">{{ $evento->id}}</td>
                                            <td>{{ $evento->titulo }}</td>
                                            <td>
                                                <form action="{{ route('eventos.destroy', $evento->id ) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input type="hidden" name="id" value="{{ $evento->id }}">
                                                    <button type="submit" class="btn btn-primary">Eliminar</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection
