@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="container-fluid">
            <div class="con-titulo">
                <h1 class="tuclase">{{ $datos['noticias']->titulo }}</h1>
            </div>
            @foreach ($datos['img_noticias'] as $img_noticia)
                    @if ($img_noticia->order==1)
                        <img class="img-descrp" src="{{ asset($img_noticia->ruta_imagen) }}" alt="">
                    @endif
                    <p style="line-height: calc(1em + 3px);">{{ substr($datos['noticias']->descripcion,0,255) }}</p>
                    @if ($img_noticia->order>1)
                        <img class="img-descrp" src="{{ asset($img_noticia->ruta_imagen) }}" alt="">
                    @endif
                    <p style="line-height: calc(1em + 3px);">{{ substr($datos['noticias']->descripcion,255) }}</p>
            @endforeach
        </div>
    </div>
@endsection
