<?php

namespace App;
use App\Imagen_Evento;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    //
    protected $fillable = [
        'titulo','descripcion','fecha_evento',
    ];

    public function imagen_evento(){
        return $this->hasMany(Imagen_Evento::class);
    }
}
