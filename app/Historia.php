<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historia extends Model
{
    //
    protected $fillable = [
        'titulo','descripcion',
    ];

}
