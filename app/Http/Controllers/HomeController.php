<?php

namespace App\Http\Controllers;
use App\Notica;
use App\Imagen_Noticia;
use App\Evento;
use App\Imagen_Evento;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $noticias=Notica::all()->sortByDesc('created_at')->take(3);
        $img_noticias=Imagen_Noticia::all();
        $eventos=Evento::all()->sortByDesc('created_at')->take(3);
        $img_eventos=Imagen_Evento::all();
        $datos=[
            'noticias'=>$noticias,
            'img_noticias'=>$img_noticias,
            'eventos'=>$eventos,
            'img_eventos'=>$img_eventos
        ];
        return view('home',compact('datos'));
    }
}
