<?php

namespace App\Http\Controllers;

use App\Evento;
use App\Imagen_Evento;
use Illuminate\Http\Request;

class Imagen_EventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $titulo=Evento::find($request->evento_id)->titulo;

        //$request->validate([
        //   'ruta_imagen'=>'required|image|mimes:jpg,png,jpeg',
        //    'evento_id'=>'required',
        //    'order'=>'required',
        //]);

        $carpeta = 'img/evento/'.$titulo;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }

        $imageName=request()->ruta_imagen->getClientOriginalName();
        request()->ruta_imagen->move(public_path($carpeta),$imageName);

        $imagen_eventos = Imagen_Evento::create($request->except('_token'));
        $imagen_eventos->ruta_imagen=$carpeta."/".request()->ruta_imagen->getClientOriginalName();
        $imagen_eventos->save();
        return redirect('/eventos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
