<?php

namespace App\Http\Controllers;

use App\Historia;
use App\Imagen_Historia;
use App\Evento;
use App\Imagen_Evento;
use App\Notica;
use App\Imagen_Noticia;
use App\User;
use Illuminate\Http\Request;

class AdministradorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $historias=Historia::all();
        $img_historias=Imagen_Historia::all();
        $eventos=Evento::all();
        $img_eventos=Imagen_Evento::all();
        $noticias=Notica::all();
        $img_noticias=Imagen_Noticia::all();
        $users=User::all();
        $datos=[
            'historias'=>$historias,
            'img_historias'=>$img_historias,
            'eventos'=>$eventos,
            'img_eventos'=>$img_eventos,
            'noticias'=>$noticias,
            'img_noticias'=>$img_noticias,
            'users'=>$users
        ];
        return view('administradores.index',compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
