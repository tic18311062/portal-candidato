<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imagen_Noticia;
use App\Notica;
class Imagen_NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //echo "entro aqui";
        //exit();
        $titulo=Notica::find($request->noticia_id)->titulo;
        $request->validate([

            'ruta_imagen'=>'required|image|mimes:jpg,png,jpeg',
            'noticia_id'=>'required',
            'order'=>'required',
        ]);
        $carpeta = 'img/noticia/'.$titulo;
        //echo "entro aqui"." ".$_POST['noticia_id']." ".$titulo.""." ".$_POST['order']." ".$carpeta;
       //exit();
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }

        $imageName=request()->ruta_imagen->getClientOriginalName();
        request()->ruta_imagen->move(public_path($carpeta),$imageName);

        $imagen_noticias = Imagen_Noticia::create($request->except('_token'));
        $imagen_noticias->ruta_imagen=$carpeta."/".request()->ruta_imagen->getClientOriginalName();
        $imagen_noticias->save();
        return redirect('/noticias');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
