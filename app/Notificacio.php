<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacio extends Model
{
    //
    protected $fillable = [
        'url_seccion','usuario_id','noticias_id','eventos_id'
    ];
}
